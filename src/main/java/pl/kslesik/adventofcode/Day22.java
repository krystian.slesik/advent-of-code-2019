package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.*;
import org.apache.commons.lang3.ArrayUtils;


public class Day22 {
    private static final Map<Command, BiConsumer<Integer, int[]>> COMMANDS_EXECUTION = Map.of(
        Command.CUT, (data, stack) -> ArrayUtils.shift(stack, -data),
        Command.DEAL_INTO_NEW_STACK, (data, stack) -> ArrayUtils.reverse(stack),
        Command.DEAL_WITH_INCREMENT, Day22::dealWithIncrement
    );

    public static void main(String[] args) {
        int[] stack = IntStream.range(0, 10007).toArray();
        List<CommandData> commands = Utils.getLines().stream()
                .map(CommandData::new)
                .collect(Collectors.toList());

        for(CommandData commandData : commands)
            COMMANDS_EXECUTION.get(commandData.getCommand()).accept(commandData.data, stack);

        System.out.println(ArrayUtils.indexOf(stack, 2019));
    }

   public static void dealWithIncrement(int data, int[] stack) {
       int[] newStack = new int[stack.length];
        for(int i=0, j=0;i<stack.length;++i, j+=data) {
            newStack[j % stack.length] = stack[i];
        }
        System.arraycopy(newStack, 0, stack, 0, stack.length);
    }

    @Getter
    private static class CommandData {
        private final Command command;
        private Integer data;

        private CommandData(String commandLine) {
            this.command = Command.valueOf(commandLine.replaceAll("[^A-z\\s]", "").trim().replaceAll("\\s", "_").toUpperCase());
            if(commandLine.matches("[A-z\\s]+-?\\d+"))
                this.data = Integer.parseInt(commandLine.replaceAll("[A-z\\s]", ""));
        }
    }

    public enum Command {
        CUT, DEAL_WITH_INCREMENT, DEAL_INTO_NEW_STACK
    }
}