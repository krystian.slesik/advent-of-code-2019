package pl.kslesik.adventofcode;

import lombok.*;
import org.paukov.combinatorics3.Generator;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.*;
import java.util.stream.*;

import static java.util.List.of;

public class Day7 {
    private static final long[] code = Utils.getIntcode();

    public static void main(String[] args) {
        partA();
        partB();
    }

    private static void partA() {
        List<Long> input = of(0L, 1L, 2L, 3L, 4L);
        findMaxTrustForce(input, Day7::getTrustersForce).ifPresent(System.out::println);
    }

    private static void partB() {
        List<Long> input = of(9L, 8L, 7L, 6L, 5L);
        findMaxTrustForce(input, Day7::getTrustersForceInLoop).ifPresent(System.out::println);
    }

    private static OptionalLong findMaxTrustForce(List<Long> input, ToLongFunction<LinkedList<Long>> getTrustForce) {
        return Generator.permutation(input)
                .simple().stream()
                .map(LinkedList::new)
                .mapToLong(getTrustForce).max();
    }

    private static long getTrustersForce(LinkedList<Long> combination) {
        AtomicLong outputs = new AtomicLong();
        while (!combination.isEmpty()) {
            new IntcodeComputer(code.clone(), new LinkedList<>(of(combination.remove(), outputs.get()))::poll, outputs::set).compute();
        }
        return outputs.get();
    }

    @SneakyThrows
    private static long getTrustersForceInLoop(LinkedList<Long> combination) {
        List<LinkedBlockingDeque<Long>> inputs = Stream.generate(LinkedBlockingDeque<Long>::new).limit(5).peek(e -> e.add(combination.poll())).collect(Collectors.toList());
        inputs.get(0).add(0L);

        ExecutorService executorService = Executors.newFixedThreadPool(inputs.size());
        IntStream.range(0, inputs.size()).mapToObj(computer -> new IntcodeComputer(code.clone(), take(inputs.get(computer)), inputs.get((computer + 1) % inputs.size())::offer))
                .forEach(executorService::submit);

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        return inputs.get(0).getFirst();
    }

    @SneakyThrows
    private static Supplier<Long> take(LinkedBlockingDeque<Long> queue) {
        return () -> {
            try {
                return queue.take();
            } catch (Exception ignored) {
                return -1L;
            }
        };
    }
}