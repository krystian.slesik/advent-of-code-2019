package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;

import static java.util.stream.Collectors.*;

public class Day3 {

    public static void main(String[] args)  {
        List<List<String>> wires = Utils.getLines().stream()
                .map(e -> Arrays.asList(e.split(",")))
                .collect(toList());

        Map<Point, Map<Integer, Collection<Integer>>> points = new HashMap<>();
        Set<Point> commonPoints = new HashSet<>();

        for (int wire = 0; wire < wires.size(); ++wire) {
            int wireLength = 1;
            List<String> moves = wires.get(wire);
            Point point = new Point(0, 0);
            for (String move : moves) {
                int pointsToMove = Integer.parseInt(move.substring(1));
                for (int i = 0; i < pointsToMove; ++i, ++wireLength) {
                    switch (move.charAt(0)) {
                        case 'U':
                            point.y += 1;
                            break;
                        case 'D':
                            point.y -= 1;
                            break;
                        case 'L':
                            point.x -= 1;
                            break;
                        case 'R':
                            point.x += 1;
                            break;
                    }
                    if (points.containsKey(point) && !points.get(point).containsKey(wire)) {
                        commonPoints.add(point);
                    }
                    points.computeIfAbsent(point, (a) -> new HashMap<>());
                    points.get(point).computeIfAbsent(wire, e -> new ArrayList<>());
                    points.get(point).get(wire).add(wireLength);
                    point = new Point(point.x, point.y);
                }
            }
        }

        System.err.println(commonPoints.stream()
                .mapToInt(point -> Math.abs(point.x) + Math.abs(point.y))
                .min());

        System.err.println(commonPoints.stream().mapToInt(
                point -> points.get(point).values().stream().mapToInt(Collections::min).sum()
        ).min());
    }

    @Data
    @AllArgsConstructor
    private static class Point {
        private int x, y;
    }


}