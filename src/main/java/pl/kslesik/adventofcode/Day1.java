package pl.kslesik.adventofcode;

import java.util.Collection;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

public class Day1 {

    public static void main(String[] args) {
        Collection<Long> data = Utils.getLines().stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());

        System.out.println(partA(data));
        System.out.println(partB(data));
    }

    public static long partA(Collection<Long> data) {
        return data.stream()
                .mapToLong(getFuelNeedByRecursion(false))
                .sum();
    }

    public static long partB(Collection<Long> data) {
        return data.stream()
                .mapToLong(getFuelNeedByIteration(true))
                .sum();
    }

    private static ToLongFunction<Long> getFuelNeedByRecursion(boolean includeFuelMass) {
        return data -> {
            long fuelNeed = Math.max((long)Math.floor(data/3D)-2, 0);
            return includeFuelMass && fuelNeed > 0 ? fuelNeed+getFuelNeedByRecursion(true).applyAsLong(fuelNeed) : fuelNeed;
        };
    }

    private static ToLongFunction<Long> getFuelNeedByIteration(boolean includeFuelMass) {
        return data -> {
            long totalFuel = 0;
            long fuelNeed = (long)Math.floor(data/3D)-2;
            do {
                totalFuel += fuelNeed;
                fuelNeed = (long)Math.floor(fuelNeed/3D)-2;
            } while(includeFuelMass && fuelNeed > 0);

            return totalFuel;
        };
    }
}
