package pl.kslesik.adventofcode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Day4 {

    public static void main(String[] args) {
        List<String> inputs = IntStream.rangeClosed(240298, 784956)
                .mapToObj(String::valueOf)
                .collect(toList());

        System.err.println(inputs.stream().filter(partA).count());
        System.err.println(inputs.stream().filter(partB).count());
    }

    private static Predicate<String> partA = code -> meetCriteria(code, false);
    private static Predicate<String> partB = code -> meetCriteria(code, true);

    private static boolean meetCriteria(String code, boolean onlyTwoSameNumbersInRow) {
        Map<Integer, Boolean> sameCharacters = new HashMap<>();
        int[] split = code.chars().toArray();
        for(int i=0;i<split.length-1;++i) {
            if(split[i] > split[i+1]) {
                return false;
            }
            if(split[i] == split[i+1]) {
                sameCharacters.merge(split[i], true, (a, b) -> !onlyTwoSameNumbersInRow);
            }
        }
        return sameCharacters.containsValue(true);
    }

}