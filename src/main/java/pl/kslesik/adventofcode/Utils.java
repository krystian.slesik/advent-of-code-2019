package pl.kslesik.adventofcode;

import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Stream.of;

public class Utils {

    public static Collection<String> getLines() {
       Scanner scanner = new Scanner(System.in);
       return Stream.generate(scanner::nextLine)
                .takeWhile(StringUtils::isNotBlank)
                .collect(Collectors.toList());
    }

    public static String[] getLinesArray() {
        Scanner scanner = new Scanner(System.in);
        return Stream.generate(scanner::nextLine)
                .takeWhile(StringUtils::isNotBlank)
                .toArray(String[]::new);
    }

    @SneakyThrows
    public static List<String> getFromDayFile(Class<?> dayClass) {
        return IOUtils.readLines(Utils.class.getResourceAsStream(String.format("/%s.txt", dayClass.getSimpleName().toLowerCase())), StandardCharsets.UTF_8);
    }

    public static long[] getIntcode() {
        return of(Utils.getLine().split(","))
                .mapToLong(Long::valueOf)
                .toArray();
    }

    public static String getLine() {
        Scanner scanner = new Scanner(new BufferedInputStream(System.in));
        return scanner.nextLine();
    }
}
