package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class Day12 {

    public static Collection<String[]> getMoonsPositions() {
        return Utils.getLines().stream()
                .map(s -> s.replaceAll("[^\\d-,]", ""))
                .map(s -> s.split(","))
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        Collection<String[]> moonsPositions = getMoonsPositions();

        partA(moonsPositions, 1000);
        partB(moonsPositions);
    }

    private static void partA(Collection<String[]> moonsPositions, long iterations) {
        List<Moon> moons = moonsPositions.stream().map(Moon::new).collect(Collectors.toList());
        for(int iteration=1;iteration<=iterations;++iteration) {
            estimateNewPositions(moons);
        }
        System.out.println(moons.stream().mapToLong(Moon::calculateTotalEnergy).sum());
    }

    private static void partB(Collection<String[]> moonsPositions) {
        int axisCount = moonsPositions.iterator().next().length;
        System.out.println(IntStream.range(0, axisCount).mapToLong(Day12.getAxisOscillationPeriod(moonsPositions))
                .reduce(Day12::lcm));
    }

    private static long lcm(long a, long b) {
        long lcm = 0;
        while((lcm+=Math.max(a, b)) % Math.min(a, b) != 0);
        return lcm;
    }

    private static IntToLongFunction getAxisOscillationPeriod(Collection<String[]> moonsPositions) {
        return axis -> {
            List<Moon> initialMoonsWithAxis = moonsPositions.stream().map(Moon::new).map(Moon.withAxis(axis)).collect(Collectors.toList());
            List<Moon> moonsWithAxis = initialMoonsWithAxis.stream().map(Moon::new).collect(Collectors.toList());

            for(int iterator=2;;++iterator) {
                estimateNewPositions(moonsWithAxis);
                if(initialMoonsWithAxis.equals(moonsWithAxis))
                    return iterator;
            }
        };
    }

    private static void estimateNewPositions(List<Moon> moons) {
        for (int i = 1; i < moons.size(); ++i) {
            Moon moon = moons.get(i - 1);
            moons.stream().skip(i).forEach(moon::updateVelocity);
        }
        moons.forEach(Moon::applyVelocity);
    }

    @AllArgsConstructor
    @EqualsAndHashCode(of = "positions")
    private static class Moon {
        private int[] positions;
        private final int[] velocity = new int[3];

        private Moon(String[] positions) {
            this.positions = Stream.of(positions).mapToInt(Integer::parseInt).toArray();
        }

        public Moon(Moon moon) {
            this.positions = moon.positions.clone();
        }

        private static UnaryOperator<Moon> withAxis(Integer axis) {
            return moon -> new Moon(new int[]{moon.positions[axis]});
        }

        public void updateVelocity(Moon moon) {
            for(int axis=0;axis<positions.length;++axis) {
                int force = (int) Math.signum(moon.positions[axis] - positions[axis]);
                velocity[axis] += force;
                moon.velocity[axis] -= force;
            }
        }

        public void applyVelocity() {
            for(int axis=0;axis<positions.length;++axis) {
                positions[axis] += velocity[axis];
            }
        }

        public long calculateTotalEnergy() {
            return Math.multiplyExact(getEnergy(positions), getEnergy(velocity));
        }

        private long getEnergy(int[] energySources) {
            return Arrays.stream(energySources).map(Math::abs).sum();
        }
    }
}