package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;
import java.util.stream.Stream;

public class Day15 {

    @AllArgsConstructor
    private enum Direction {
        NORTH(1, 0, 1), SOUTH(2, 0, -1), WEST(3, -1, 0), EAST(4, 1, 0);
        private final long move;
        private final int xIncrease;
        private final int yIncrease;
    }

    private static final LinkedList<Point> positions = new LinkedList<>();
    private static final LinkedList<Direction> moves = new LinkedList<>();
    private static final Map<Point, String> elements = new LinkedHashMap<>();

    private static Point oxygen;
    private static int longestPath = 0;

    private static Long getMove() {
        if (elements.size() > 3 && positions.size() == 1) {
//            printMap();
            throw new RuntimeException("Nothing more to do in here. Bricks, everywhere bricks");
        } else {
            longestPath = Math.max(longestPath, positions.size());
            return getNextMove();
        }
    }

    private static Long getNextMove() {
        Optional<Pair<Direction, Point>> optionalNearbyPoint = getNearbyPoint(positions.getLast());
        if (optionalNearbyPoint.isPresent()) {
            Pair<Direction, Point> directionAndPoint = optionalNearbyPoint.get();
            moves.add(directionAndPoint.getKey());
            positions.add(directionAndPoint.getValue());
            return directionAndPoint.getKey().move;
        } else {
            positions.removeLast();
            switch (moves.removeLast()) {
                case NORTH:
                    return Direction.SOUTH.move;
                case SOUTH:
                    return Direction.NORTH.move;
                case WEST:
                    return Direction.EAST.move;
                default:
                    return Direction.WEST.move;
            }
        }
    }

    private static Optional<Pair<Direction, Point>> getNearbyPoint(Point last) {
        return Stream.of(Direction.values())
                .map(direction -> Pair.of(direction, new Point(last.x + direction.xIncrease, last.y + direction.yIncrease)))
                .filter(e -> !elements.containsKey(e.getValue()))
                .findAny();
    }

    private static void setOutput(Long responseCode) {
        switch (responseCode.intValue()) {
            case 0:
                elements.put(positions.removeLast(), "█");
                moves.removeLast();
                break;
            case 1:
                elements.put(positions.getLast(), " ");
                break;
            case 2:
                oxygen = positions.getLast();
                System.out.printf("Shortest path to oxygen: %s\n", moves.size());
                elements.put(positions.getLast(), "●");
                break;
        }
    }

    public static void main(String[] args) {
        positions.add(new Point(0L, 0L));
        elements.put(new Point(0L, 0L), "0");
        try {
            new IntcodeComputer(Utils.getIntcode(), Day15::getMove, Day15::setOutput).compute();
        } catch(Exception ignore) {}

        partB(); //use same algorithm to search through a map just start from oxygen point
    }

    private static void partB() {
        longestPath = 0;
        positions.clear();
        positions.add(oxygen);
        moves.clear();
        final Map<Point, String> map = new HashMap<>(elements);
        elements.clear();

        try {
            while (getMove() != -1L) {
                String s = map.get(positions.getLast());
                setOutput(s.equals("█") ? 0L : 1L);
            }
        } catch(Exception ignored) {
            System.out.printf("Longest path from oxygen(-1 equals minutes/moves): %s\n", longestPath);
        }
    }

    private static void printMap() {
        LongSummaryStatistics xSummary = elements.keySet().stream().mapToLong(Point::getX).summaryStatistics();
        LongSummaryStatistics ySummary = elements.keySet().stream().mapToLong(Point::getY).summaryStatistics();
        System.out.println();
        for (long y = ySummary.getMax(); y >= ySummary.getMin(); --y) {
            for (long x = xSummary.getMin(); x <= xSummary.getMax(); ++x) {
                var position = new Point(x, y);
                if (position.equals(positions.getFirst())) {
                    System.out.print("@");
                } else {
                    System.out.print(elements.getOrDefault(position, " "));
                }
            }
            System.out.println();
        }
        System.out.flush();
    }

    @Data
    @AllArgsConstructor
    private static class Point {
        private Long x, y;
    }

    @Data
    @AllArgsConstructor(staticName = "of")
    private static class Pair<K, V> {
        private K key;
        private V value;
    }
}