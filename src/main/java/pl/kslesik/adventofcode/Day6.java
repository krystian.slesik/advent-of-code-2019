package pl.kslesik.adventofcode;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toCollection;

public class Day6 {
    private static final String data = String.join("", Utils.getFromDayFile(Day6.class));

    private static final Map<String, String> orbits = new HashMap<>();

    public static void main(String[] args) {
        Stream.of(data.split(",")).forEach(orbitData -> {
            String[] orbit = orbitData.split("\\)");
            orbits.put(orbit[1], orbit[0]);
        });

        partA();
        partB();
    }

    private static void partA() {
        int orbitsSum = orbits.keySet().stream().map(Day6::getOrbits).mapToInt(Collection::size).sum();
        System.out.println(orbitsSum);
    }

    private static void partB() {
        LinkedList<String> san = getOrbits("SAN");
        LinkedList<String> you = getOrbits("YOU");
        while(Objects.equals(you.pollLast(), san.pollLast()));
        System.out.println(san.size() + you.size());
    }

    private static LinkedList<String> getOrbits(String object) {
        return Stream.iterate(object, Day6.orbits::containsKey,  Day6.orbits::get).collect(toCollection(LinkedList::new));
    }
}