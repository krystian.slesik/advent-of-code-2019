package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;
import java.util.function.Consumer;

import static java.lang.Math.*;

public class Day11 {

    private static final LinkedList<Long> computerInput = new LinkedList<>(){{add(1L);}};
    private static final LinkedList<Long> computerOutput = new LinkedList<>() {};

    public static void main(String[] args) {
        final Robot robot = new Robot();
        final Map<Point, Long> points = new LinkedHashMap<>();

        final Consumer<Long> computeOutput = output -> {
            computerOutput.add(output);
            if (computerOutput.size() == 2) {
                Long color = computerOutput.poll();
                Long directionToMove = computerOutput.poll();
                points.put(robot.position, color);
                robot.move(directionToMove);
                computerInput.add(points.getOrDefault(robot.position, 0L));
            }
        };

        new IntcodeComputer(Utils.getIntcode(), computerInput::poll, computeOutput).compute();

        System.out.printf("PartA: %s\n", points.size());
        System.out.println("PartB:");
        printImage(points);
    }

    private static void printImage(Map<Point, Long> points) {
        IntSummaryStatistics xSummary = points.keySet().stream().mapToInt(Point::getX).summaryStatistics();
        IntSummaryStatistics ySummary = points.keySet().stream().mapToInt(Point::getY).summaryStatistics();
        for(int y=ySummary.getMax();y>=ySummary.getMin();--y) {
            for(int x=xSummary.getMax();x>=xSummary.getMin();--x) {
                long color = points.getOrDefault(new Point(x, y), 0L);
                System.out.print(color == 1 ? '●' : ' ');
            }
            System.out.println();
        }
    }

    @Data
    private static class Robot {
        private Point position = new Point(0, 0);
        private int direction = 90;

        public void move(Long direction) {
            this.direction += -90 + direction*180;
            position = new Point(position.x + (int)cos(toRadians(this.direction)), position.y + (int)sin(toRadians(this.direction)));
        }
    }

    @Data
    @AllArgsConstructor
    @ToString(includeFieldNames = false)
    private static class Point {
        private final int x, y;
    }
}