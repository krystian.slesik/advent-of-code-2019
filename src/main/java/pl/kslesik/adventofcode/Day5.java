package pl.kslesik.adventofcode;

public class Day5 {

    public static void main(String[] args) {
        new IntcodeComputer(Utils.getIntcode()).compute();
    }
}