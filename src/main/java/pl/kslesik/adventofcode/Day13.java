package pl.kslesik.adventofcode;

import lombok.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Day13 {

    private static final PrintStream output = new PrintStream(new BufferedOutputStream(new FileOutputStream(FileDescriptor.out), 1600000), false, StandardCharsets.UTF_8);

    private static final Point CHANGE_SCORE = new Point(-1L, 0L);
    private static final Map<Long, Character> GAME_SYMBOLS = Map.of(
            0L, ' ',
            1L, '█',
            2L, '□',
            3L, '_',
            4L, '●'
    );

    private static final LinkedList<Long> computerOutput = new LinkedList<>();
    private static final Map<Point, Long> gameElements = new LinkedHashMap<>();

    private static long score;
    private static Point ball;
    private static Point paddle;

    private static Long getMove() {
//        printGame();
        return (long)Long.compare(ball.getX(), paddle.getX());
    }

    private static void setOutput(Long number) {
        computerOutput.add(number);
        if(computerOutput.size() == 3) {
            Point point = new Point(computerOutput.poll(), computerOutput.poll());
            Long tileId = computerOutput.poll();
            if (CHANGE_SCORE.equals(point)) {
                score = tileId;
            } else {
                gameElements.put(point, tileId);
                if (tileId == 4) {
                    ball = point;
                } else if (tileId == 3) {
                    paddle = point;
                }
            }
        }
    }

    public static void main(String[] args) {
        long[] code = Utils.getIntcode();
        code[0] = 2;//to play for free. Yay!!!

        new IntcodeComputer(code, Day13::getMove, Day13::setOutput).compute();

        System.out.printf("Final score: %s\n", score);
    }

    @SneakyThrows
    private static void printGame() {
        clearScreen();
        LongSummaryStatistics xSummary = gameElements.keySet().stream().mapToLong(Point::getX).summaryStatistics();
        LongSummaryStatistics ySummary = gameElements.keySet().stream().mapToLong(Point::getY).summaryStatistics();
        output.printf("Score: %d\n", score);
        for(long y=0;y<=ySummary.getMax();++y){
            for (long x=0;x<=xSummary.getMax();++x) {
                output.print(GAME_SYMBOLS.get(gameElements.getOrDefault(new Point(x, y), 0L)));
            }
            output.println();
        }
        output.flush();
        Thread.sleep(1000/20);
    }

    @SneakyThrows
    public static void clearScreen() {
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
    }

    @Data
    @AllArgsConstructor
    private static class Point {
        private Long x, y;
    }
}