package pl.kslesik.adventofcode;

import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import static java.util.Comparator.comparing;
import static java.util.function.Predicate.*;
import static java.util.stream.Collectors.*;
import static java.util.stream.Stream.of;

public class Day17 {

    private static final long[] code = Utils.getIntcode();
    private static final Map<Point, Character> elements = new HashMap<>();
    private static final Map<Character, Double> angles = Map.of(
        '>', 0D,
        '^', Math.PI/2,
        'v', -Math.PI/2,
        '<', Math.PI
    );

    private static Consumer<Long> setOutput() {
        int[] xy = new int[2];
        return responseCode -> {
            if (responseCode == 10) {
                xy[0] = 0;
                xy[1] += 1;
            } else {
                elements.put(new Point(xy[0]++, xy[1]), (char) responseCode.intValue());
            }

            System.out.print((char)responseCode.intValue());
        };
    }

    public static void main(String[] args) {
        partA();
        partB();
    }

    private static void partA() {
        new IntcodeComputer(code, null, setOutput()).compute();
        System.out.println(elements.entrySet().stream().filter(Day17::isIntersection).map(Map.Entry::getKey)
                .mapToInt(Point::distanceToZero).sum());
    }

    private static void partB() {
        Collection<String> moves = getMoves();
        String mainRoutine = StringUtils.join(moves, ",");
        Collection<String> subcommands = splitToSubcommands(mainRoutine);
        LinkedList<String> functionsNames = of("ABC".split("")).collect(toCollection(LinkedList::new));
        for(String subcommand : subcommands)
            mainRoutine = mainRoutine.replace(subcommand, functionsNames.removeFirst());

        LinkedList<Long> commands = Stream.of(mainRoutine, String.join("\n", subcommands), "n").collect(joining("\n", "", "\n")).chars()
                .mapToObj(Long::valueOf).collect(toCollection(LinkedList::new));

        code[0] = 2;
        new IntcodeComputer(code, commands::poll, output -> {
            if(output > 0xFF) {
                System.out.println(output);
            } else {
                System.out.print((char)output.intValue());
            }
        }).compute();
    }

    private static Collection<String> getMoves() {
        List<String> path = new LinkedList<>();
        Point previous = elements.entrySet().stream().filter(e -> angles.containsKey(e.getValue())).findFirst().map(Map.Entry::getKey).orElseThrow(); //start from robot position
        double angle = angles.get(elements.get(previous));
        elements.values().removeIf(not(isEqual('#')));
        Set<Point> positions = new LinkedHashSet<>();
        while (elements.size() != positions.size()) {
            Point next = previous.getNearbyPoints().filter(not(positions::contains)).filter(elements::containsKey).findFirst().orElseThrow();
            double nextAngle = Math.atan2(previous.y - next.y, next.x - previous.x);
            path.add(Math.sin(nextAngle - angle) == 1D ? "L" : "R");
            angle = nextAngle;
            int moves = 0;
            while (elements.containsKey(next)) {
                positions.add(next);
                moves++;
                previous = next;
                next = next.nextInLine(angle);
            }
            path.add(String.valueOf(moves));
        }
        return path;
    }

    private static Collection<String> splitToSubcommands(String pathCommand) {
        List<String> subcommands = new LinkedList<>();
        while(!pathCommand.isEmpty()) {
            String command = pathCommand;
            String subcommand = IntStream.rangeClosed(10, 20)
                    .mapToObj(i -> command.substring(0, Math.min(i, command.length())))
                    .max(comparing(countOccurances(pathCommand)).thenComparing(String::length))
                    .orElseThrow();
            subcommands.add(subcommand);
            pathCommand = StringUtils.strip(StringUtils.remove(pathCommand, subcommand), ",").replace(",,", ",");
        }
        return subcommands;
    }

    private static Function<String, Integer> countOccurances(String command) {
        return subcommand -> StringUtils.countMatches(command, subcommand);
        
    }

    private static boolean isIntersection(Map.Entry<Point, Character> entry) {
        Point point = entry.getKey();
        return entry.getValue() == '#' && point.getNearbyPoints().map(elements::get).allMatch(isEqual('#'));
    }

    @Data
    @AllArgsConstructor
    private static class Point {
        private int x, y;

        public Stream<Point> getNearbyPoints() {
            return IntStream.rangeClosed(0, 360).mapToDouble(Math::toRadians).mapToObj(this::nextInLine);
        }

        public int distanceToZero() {
            return x * y;
        }

        public Point nextInLine(double angle) {
            return new Point(x + (int) Math.cos(angle), y - (int) Math.sin(angle));
        }
    }
}