package pl.kslesik.adventofcode;

import java.util.*;
import java.util.function.Supplier;

import static java.util.stream.Collectors.*;

public class Day21 {
    public static void main(String[] args) {
        new IntcodeComputer(Utils.getIntcode(), getInput(), output -> {
            if(output > 0xFF) {
                System.out.println(output);
            } else {
                System.out.print((char)output.intValue());
            }
        }).compute();
    }

    private static Supplier<Long> getInput() {
        Collection<String> instructions = List.of(
                "NOT A J",
                "NOT B T",
                "OR J T",
                "NOT C J",
                "OR J T", // !(A && B && C)

                "NOT D J",
                "NOT J J",

                "AND T J",

                "NOT H T", //partB
                "NOT T T",
                "OR E T",

                "AND T J",
                "RUN"
        );
        LinkedList<Long> collect = instructions.stream().collect(joining("\n", "", "\n")).chars().mapToObj(Long::valueOf).collect(toCollection(LinkedList::new));
        return collect::poll;

    }
}
