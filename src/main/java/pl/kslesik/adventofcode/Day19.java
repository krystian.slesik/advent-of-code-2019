package pl.kslesik.adventofcode;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day19 {

    private static final long[] code = Utils.getIntcode();

    public static void main(String[] args) {
        partA();
        partB();
    }

    public static void partA() {
        long sum = 0;
        for(long i=0;i<50*50;++i) {
            if (checkBeam(i%50, i/50))
                sum++;
        }
        System.out.println(sum);
    }

    private static void partB() {
        long[] longs = find(100, 30); //y must be at least 100 to fit 100x100 square
        longs = find(longs[1]-30, 1);
        System.out.println(longs[0]*10000 + longs[1]);
    }

    private static long[] find(long yStart, long yOffset) {
        long x, y;
        boolean trackBeam = false;
        double proportionXtoY = getXtoYproportion();
        for(y=yStart;;y+=yOffset) {
            for(x=(int)(y*proportionXtoY);;++x) {
                boolean beam = checkBeam(x, y);
                if(!trackBeam) {
                    trackBeam = beam;
                } else if(!beam || !checkBeam(x+99, y)) {
                    break;
                }
                if(checkBeam(x, y+99)) {
                    return new long[]{x, y};
                }
            }
        }
    }

    private static double getXtoYproportion() {
        for(long x=0;;x++) {
            if(checkBeam(x, 100))
                return x/100D;
        }
    }

    private static boolean checkBeam(long x, long y) {
        AtomicLong result = new AtomicLong();
        LinkedList<Long> coordinates = Stream.of(x, y).collect(Collectors.toCollection(LinkedList::new));
        new IntcodeComputer(code, coordinates::poll, result::set).compute();
        return result.get() == 1;
    }
}