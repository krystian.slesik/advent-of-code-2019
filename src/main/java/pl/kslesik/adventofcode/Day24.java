package pl.kslesik.adventofcode;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import static java.util.function.Predicate.isEqual;
import static java.util.stream.Collectors.toMap;

public class Day24 {

    private static final int MAP_SIZE = 5;
    private static final Point CENTER = new Point(MAP_SIZE>>1, MAP_SIZE>>1);

    public static void main(String[] args) {
        partA();
        partB();
    }

    private static void partA() {
        Stream.iterate(getBaseMap(), Day24::mapA).dropWhile(new HashSet<>()::add).findFirst()
                .map(Day24::getBiodiversity)
                .ifPresent(System.out::println);
    }

    private static Map<Point, Character> mapA(Map<Point, Character> oldMap) {
        return evolve(oldMap, point -> point.getNearbyPoints().map(oldMap::get));
    }

    private static void partB() {
        int minutes = 201;
        long bugs = Stream.iterate(new HashMap<>(){{put(0, getBaseMap());}}, Day24::mapB).limit(minutes).skip(minutes - 1)
                .map(Map::values).flatMap(Collection::stream)
                .map(Map::values).flatMap(Collection::stream)
                .filter(isEqual('#')).count();
        System.out.println(bugs);
    }

    private static Map<Integer, Map<Point, Character>> mapB(Map<Integer, Map<Point, Character>> levelsMap) {
        IntSummaryStatistics levels = levelsMap.keySet().stream().mapToInt(Integer::intValue).summaryStatistics();
        levelsMap.put(levels.getMin()-1, newLevel());
        levelsMap.put(levels.getMax()+1, newLevel());

        return levelsMap.entrySet().stream().collect(toMap(Map.Entry::getKey, mapLevel -> evolve(mapLevel.getValue(), getBugsMultiLevel(mapLevel.getKey(), levelsMap))));
    }

    private static Function<Point, Stream<Character>> getBugsMultiLevel(Integer level, Map<Integer, Map<Point, Character>> levelsMap) {
        return basePoint -> basePoint.getNearbyPoints().flatMap(point -> {
            if (basePoint.equals(CENTER)) {
                return Stream.empty();
            } else if (point.equals(CENTER)) {
                if(!levelsMap.containsKey(level+1)) {
                    return Stream.empty();
                } else if (basePoint.getX() == 2) {
                    return IntStream.range(0, MAP_SIZE).mapToObj(x -> new Point(x, basePoint.getY() == 1 ? 0 : 4)).map(levelsMap.get(level+1)::get);
                } else {
                    return IntStream.range(0, MAP_SIZE).mapToObj(y -> new Point(basePoint.getX() == 1 ? 0 : 4, y)).map(levelsMap.get(level+1)::get);
                }
            } else if (levelsMap.getOrDefault(level, Collections.emptyMap()).containsKey(point)) {
                return Stream.ofNullable(levelsMap.get(level).get(point));
            } else {
                Point newPoint = new Point(point.getX() == 5 ? 3 : point.getX() == -1 ? 1 : 2, point.getY() == 5 ? 3 : point.getY() == -1 ? 1 : 2);
                return Stream.ofNullable(levelsMap.getOrDefault(level - 1, Collections.emptyMap()).get(newPoint));
            }
        });
    }

    private static Map<Point, Character> evolve(Map<Point, Character> map, Function<Point, Stream<Character>> getAdjacentTiles) {
        return map.entrySet().stream().collect(toMap(Map.Entry::getKey, getNewValue(getAdjacentTiles)));
    }

    private static Function<Map.Entry<Point, Character>, Character> getNewValue(Function<Point, Stream<Character>> getAdjacentTiles) {
        return entry -> {
            long bugsCount = getAdjacentTiles.apply(entry.getKey()).filter(isEqual('#')).count();
            if (entry.getValue() == '#') {
                return bugsCount == 1 ? '#' : '.';
            } else {
                return bugsCount == 1 || bugsCount == 2 ? '#' : '.';
            }
        };
    }

    private static Map<Point, Character> getBaseMap() {
        Map<Point, Character> elements = new HashMap<>();

        List<String> fromDayFile = Utils.getFromDayFile(Day24.class);
        for (int y = 0; y < fromDayFile.size(); ++y) {
            for (int x = 0; x < fromDayFile.get(y).length(); ++x) {
                elements.put(new Point(x, y), fromDayFile.get(y).charAt(x));
            }
        }
        return elements;
    }

    private static Map<Point, Character> newLevel() {
        Map<Point, Character> elements = new HashMap<>();
        for (int y = 0; y < MAP_SIZE; ++y)
            for (int x = 0; x < MAP_SIZE; ++x)
                elements.put(new Point(x, y), '.');
        return elements;
    }

    private static long getBiodiversity(Map<Point, Character> pointCharacterMap) {
        pointCharacterMap.values().removeIf(isEqual('.'));
        return pointCharacterMap.keySet().stream().mapToLong(point -> (long) Math.pow(2, point.getY() * 5 + point.getX())).sum();
    }
}