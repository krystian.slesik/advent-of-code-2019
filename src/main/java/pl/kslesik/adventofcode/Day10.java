package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;
import java.util.function.Predicate;

import static java.util.Comparator.*;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.*;

public class Day10 {
    private static Set<Point> asteroids = new LinkedHashSet<>();

    public static void main(String[] args) {
        initAsteroidsPositions();
        partA();
        partB();
    }

    private static void partA() {
        System.out.println(asteroids.stream().mapToLong(Day10::getVisibleAsteroidsCount).max());
    }

    private static void partB() {
        Point max = asteroids.stream().max(comparing(Day10::getVisibleAsteroidsCount)).get();
        Map<Double, LinkedList<Point>> pointsByAngle = asteroids.stream()
                .filter(not(max::equals))
                .sorted(comparing(max::manhattanDistance))
                .collect(groupingBy(max::getAngle, toCollection(LinkedList::new)));
        Double[] angles = pointsByAngle.keySet().stream().sorted().toArray(Double[]::new);

        LinkedList<Point> removedPoints = new LinkedList<>();

        int i=0;
        while(removedPoints.size() != 200) {
            LinkedList<Point> points = pointsByAngle.get(angles[i++ % angles.length]);
            ofNullable(points.poll()).ifPresent(removedPoints::add);
        }
        System.out.println(removedPoints.getLast());
    }

    private static long getVisibleAsteroidsCount(Point point) {
        return asteroids.stream()
                .filter(not(point::equals))
                .filter(checkVisibility(point))
                .count();
    }

    private static Predicate<? super Point> checkVisibility(Point from) {
        Map<Double, Set<Point>> byAngle = asteroids.stream()
                .filter(not(from::equals))
                .collect(groupingBy(from::getAngle, toSet()));
        return to -> byAngle.get(from.getAngle(to)).stream().map(from::manhattanDistance).noneMatch(distance -> distance < from.manhattanDistance(to));
    }

    private static void initAsteroidsPositions() {
        String[] lines = Utils.getLinesArray();
        for(int y=0;y<lines.length;++y) {
            for(int x=0;x<lines[y].length();++x) {
                if(lines[y].charAt(x) == '#')
                    asteroids.add(Point.of(x, y));
            }
        }
    }

    @Data
    @AllArgsConstructor(staticName = "of", access = AccessLevel.PRIVATE)
    public static class Point {
        private int x, y;

        public Double getAngle(Point other) {
            double angle = 90-Math.toDegrees(Math.atan2(y-other.y, other.x-x));
            return angle < 0 ? angle+360 : angle;
        }

        public long manhattanDistance(Point other) {
            return Math.abs(x-other.x) + Math.abs(y-other.y);
        }
    }
}