package pl.kslesik.adventofcode;

import lombok.Value;

import java.util.stream.IntStream;
import java.util.stream.Stream;

@Value
public class Point {
    private final int x, y;

    public Stream<Point> getNearbyPoints() {
        return IntStream.iterate(0, angle -> angle + 90).limit(4).mapToDouble(Math::toRadians).mapToObj(this::nextInLine);
    }

    public Point nextInLine(double angle) {
        return new Point(x + (int) Math.cos(angle), y - (int) Math.sin(angle));
    }
}