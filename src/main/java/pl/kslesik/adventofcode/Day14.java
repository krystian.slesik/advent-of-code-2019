package pl.kslesik.adventofcode;

import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day14 {

    private static Map<String, Set<Resource>> resources;
    private static Map<Resource, Long> productionMultiplicity = new HashMap<>();

    public static void main(String[] args) {
        resources = Utils.getLines().stream()
                .map(StringUtils::deleteWhitespace)
                .collect(Collectors.toMap(Day14::getResource, Day14::getRequiredResources));

        System.out.println(partA(1));
        System.out.println(partB(1E12));
    }

    private static long partB(double totalOre) {
        int low = 0, mid = -1, high = (int)1E12;
        while(low <= high) {
            mid = (low+high)>>>1;
            long midVal = partA(mid);
            if(midVal < totalOre) {
                low = mid+1;
            } else if (midVal > totalOre){
                high = mid-1;
            } else {
                return mid;
            }
        }
        return mid-1;
    }

    private static Long partA(int quantity) {
        return resources.get("FUEL").stream()
                .mapToLong(getORENeed(resources, new HashMap<>(), quantity))
                .sum();
    }

    private static ToLongFunction<Resource> getORENeed(final Map<String, Set<Resource>> resources, final Map<String, Long> leftovers, int produceCount) {
        return resource -> {
            if ("ORE".equals(resource.getResource())) {
                return produceCount * resource.getNeed();
            } else {
                long quantityNeed = produceCount * resource.getNeed() - leftovers.getOrDefault(resource.getResource(), 0L);
                long productionMultiplicity = Day14.productionMultiplicity.get(resource);
                int create = (int) Math.ceil(quantityNeed / (double) productionMultiplicity);
                long leftover = productionMultiplicity * create - quantityNeed;
                leftovers.compute(resource.getResource(), (key, oldValue) -> leftover == 0 ? null : leftover);
                return resources.get(resource.getResource()).stream().mapToLong(getORENeed(resources, leftovers, create)).sum();
            }
        };
    }

    private static Set<Resource> getRequiredResources(String g) {
        return Stream.of(g.split("=>")[0])
                .map(resource -> resource.split(","))
                .flatMap(Stream::of)
                .map(Resource::new)
                .collect(Collectors.toCollection(LinkedHashSet::new));

    }

    private static String getResource(String g) {
        Resource resource = new Resource(g.split("=>")[1]);
        productionMultiplicity.put(resource, resource.getNeed());
        return resource.getResource();
    }

    @Data
    @EqualsAndHashCode(of = "resource")
    @AllArgsConstructor(staticName = "of")
    private static class Resource {
        private final String resource;
        private long need;

        private Resource(String resourceData) {
            this.need = Integer.parseInt(resourceData.replaceAll("[^\\d]", ""));
            this.resource = resourceData.replaceAll("[\\d]", "");
        }
    }
}