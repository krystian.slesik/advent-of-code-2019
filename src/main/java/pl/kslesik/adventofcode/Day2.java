package pl.kslesik.adventofcode;

public class Day2 {
    public static void main(String[] args) {
        long[] code = Utils.getIntcode();

        code[1] = 12;
        code[2] = 2;
        new IntcodeComputer(code).compute();

        System.out.println(code[0]);
    }
}
