package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

@AllArgsConstructor
@RequiredArgsConstructor
public class IntcodeComputer extends Thread {

    private static final UnaryOperator<Integer> DO_NOT_INCREASE_POINTER = pointerIncrease -> 0;

    private final long[] code;
    private Supplier<Long> input = new Scanner(System.in)::nextLong;
    private Consumer<Long> output = System.out::println;

    private final Map<Integer, Function<OperationData, Integer>> OPERATIONS = Map.of(
            1, threeArgumentOperator(Math::addExact),
            2, threeArgumentOperator(Math::multiplyExact),
            3, argumentsOperator(1, this::readInput),
            4, argumentsOperator(1, (data, args) -> Stream.of(args).forEach(output)),
            5, argumentsOperator(2, jumpIf(args -> args[0] != 0)).andThen(DO_NOT_INCREASE_POINTER),
            6, argumentsOperator(2, jumpIf(args -> args[0] == 0)).andThen(DO_NOT_INCREASE_POINTER),
            7, threeArgumentOperator((firstArgument, secondArgument) -> firstArgument < secondArgument ? 1L : 0),
            8, threeArgumentOperator((firstArgument, secondArgument) -> firstArgument.equals(secondArgument) ? 1L : 0),
            9, argumentsOperator(1, (data, args) -> data.relativeBase+=args[0])
        );

    private void readInput(OperationData data, Long[] args) {
        for(int arg=0;arg<args.length;) {
            data.setParameter(++arg, input.get());
        }
    }

    private Function<OperationData, Integer> argumentsOperator(int args, BiConsumer<OperationData, Long[]> operation) {
        return operationData -> {
            operation.accept(operationData, operationData.getParameters(args));
            return args+1;
        };
    }

    private BiConsumer<OperationData, Long[]> jumpIf(Predicate<Long[]> operator) {
        return (data, args) -> data.setPointer(operator.test(args) ? args[1].intValue() : data.pointer+3);
    }

    private Function<OperationData, Integer> threeArgumentOperator(BiFunction<Long, Long, Long> operator) {
        return argumentsOperator(3, (data, args) -> data.setParameter(3, operator.apply(args[0], args[1])));
    }

    public void run() {
        compute();
    }

    public void compute() {
        OperationData operationData = OperationData.of(code);
        while (OPERATIONS.containsKey(operationData.getOperationCode())) {
            operationData.pointer = OPERATIONS.get(operationData.getOperationCode()).apply(operationData) + operationData.pointer;
        }
    }

    @RequiredArgsConstructor(staticName = "of", access = AccessLevel.PRIVATE)
    private static class OperationData {
        @NonNull private long[] code;
        @Setter private int relativeBase = 0, pointer = 0;

        public Long[] getParameters(int parametersCount) {
            return IntStream.rangeClosed(1, parametersCount).map(this::getOffset).mapToObj(offset -> code[offset]).toArray(Long[]::new);
        }

        public void setParameter(int parameterIndex, long value) {
            int offset = getOffset(parameterIndex);
            code[offset] = value;
        }

        public int getOperationCode() {
            return (int)code[pointer] % 100;
        }

        private int getOffset(int parameterIndex) {
            long mode = getParameterMode(parameterIndex);
            if(mode == 1) {
                return this.pointer+parameterIndex;
            } else {
                int offset = (mode == 0 ? 0 : relativeBase) + (int) code[this.pointer + parameterIndex];
                if (offset >= code.length)
                    code = Arrays.copyOf(code, offset + 100);
                return offset;
            }
        }

        private long getParameterMode(int parameterIndex) {
            long parametersMode = code[pointer]/100;
            return parametersMode / (int)Math.pow(10, parameterIndex-1) % 10;
        }
    }
}