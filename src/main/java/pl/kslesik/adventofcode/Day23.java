package pl.kslesik.adventofcode;

import lombok.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;

import static java.util.Collections.*;
import static java.util.Optional.ofNullable;

public class Day23 {

    private static final long[] code = Utils.getIntcode();
    private static final Nat NAT = new Nat();
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    private static final Map<Long, LinkedList<Long>> computersInputs = new HashMap<>();
    private static final Map<Long, LinkedList<Long>> computersOutputs = new HashMap<>();

    public static void main(String[] args) {
        executor.submit(NAT);
        LongStream.range(0, 50).mapToObj(Day23::createComputer).collect(Collectors.toList()).forEach(executor::submit);
    }

    private static IntcodeComputer createComputer(long address) {
        computersInputs.put(address, new LinkedList<>(singletonList(address)));
        computersOutputs.put(address, new LinkedList<>());
        return new IntcodeComputer(code.clone(), getInput(address), setOutput(address));
    }

    private static Consumer<Long> setOutput(long address) {
        LinkedList<Long> outputBuffer = computersOutputs.get(address);
        return data -> {
            synchronized (NAT) {
                outputBuffer.add(data);
                if (outputBuffer.size() == 3) {
                    long toAddress = outputBuffer.remove();
                    if (toAddress == Nat.ADDRESS) {
                        NAT.set(outputBuffer.remove(), outputBuffer.remove());
                    } else {
                        computersInputs.get(toAddress).add(outputBuffer.remove());
                        computersInputs.get(toAddress).add(outputBuffer.remove());
                    }
                }
                NAT.notify();
            }
        };
    }

    private static Supplier<Long> getInput(long address) {
        LinkedList<Long> input = computersInputs.get(address);
        return () -> {
            synchronized (NAT) {
                Long inData = ofNullable(input.pollFirst()).orElse(-1L);
                NAT.notify();
                return inData;
            }
        };
    }

    private static class Nat implements Runnable {
        private static final int ADDRESS = 255;
        private Long lastSendY;
        private List<Long> data = emptyList();

        @SneakyThrows
        public synchronized void run() {
            while (true) {
                while(data.isEmpty() || !isIdle())
                    wait();
                if (Objects.equals(lastSendY, data.get(1))) {
                    System.out.println("PartB solution: " + data.get(1));
                    break;
                }
                System.out.printf("NAT send %s\n", data);
                computersInputs.get(0L).addAll(data);
                lastSendY = data.get(1);
            }
        }

        public void set(Long x, Long y) {
            this.data = List.of(x, y);
        }

        private boolean isIdle() {
            return Stream.of(computersOutputs.values(), computersInputs.values()).flatMap(Collection::stream).allMatch(Collection::isEmpty);
        }
    }
}